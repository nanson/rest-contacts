package com.example.rest.controller;

import com.example.rest.dto.ApplicationDto;
import com.example.rest.exception.NotFoundException;
import com.example.rest.service.ApplicationService;
import com.example.rest.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/contacts/{contactId}/applications",
        produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class ApplicationRestController {

    private final ApplicationService applicationService;

    private final ContactService contactService;

    @Autowired
    public ApplicationRestController(ApplicationService applicationService, ContactService contactService) {
        this.applicationService = applicationService;
        this.contactService = contactService;
    }

    @GetMapping(path = "/latest")
    public ApplicationDto getContactLatest(@PathVariable("contactId") Long contactId) {
        contactService.findOne(contactId)
                .orElseThrow(() -> new NotFoundException(String.format("Couldn't find contact with id %d", contactId)));

        return applicationService.findContactLast(contactId)
                .orElseThrow(() -> new NotFoundException(String.format("Couldn't find applications for contact with id = %d", contactId)));

    }
}
