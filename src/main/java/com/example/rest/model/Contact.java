package com.example.rest.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "CONTACT")
public class Contact {
    @Id
    @GeneratedValue(generator = "SEQ_CONTACT", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_CONTACT", sequenceName = "SEQ_CONTACT", allocationSize = 1)
    @Column(name = "CONTACT_ID")
    private Long id;

    @OneToMany(mappedBy = "contact", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Application> applications = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Application> getApplications() {
        return applications;
    }

    public void setApplications(Set<Application> applications) {
        this.applications = applications;
    }
}
