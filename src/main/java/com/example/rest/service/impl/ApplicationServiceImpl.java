package com.example.rest.service.impl;

import com.example.rest.dto.ApplicationDto;
import com.example.rest.repository.ApplicationRepository;
import com.example.rest.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    private final ApplicationRepository applicationRepository;

    @Autowired
    public ApplicationServiceImpl(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<ApplicationDto> findContactLast(long contactId) {
        return applicationRepository.findTopByContactIdOrderByCreatedAtDesc(contactId)
                .map(application -> {
                    ApplicationDto applicationDto = new ApplicationDto();
                    applicationDto.setId(application.getId());
                    applicationDto.setCreatedAt(application.getCreatedAt());
                    applicationDto.setProductName(application.getProductName());
                    applicationDto.setContactId(application.getContact().getId());
                    return applicationDto;
                });
    }
}
