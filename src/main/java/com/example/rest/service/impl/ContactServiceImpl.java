package com.example.rest.service.impl;

import com.example.rest.model.Contact;
import com.example.rest.repository.ContactRepository;
import com.example.rest.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContactServiceImpl implements ContactService {

    private final ContactRepository contactRepository;

    @Autowired
    public ContactServiceImpl(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Contact> findOne(long id) {
        return contactRepository.findById(id);
    }
}
