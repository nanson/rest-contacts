package com.example.rest.service;

import com.example.rest.dto.ApplicationDto;

import java.util.Optional;

/**
 * Сервис заказов контактов
 */
public interface ApplicationService {

    /**
     * Возвращает последний заказ клиента
     *
     * @param contactId идентификатор клиента
     * @return заказ
     */
    Optional<ApplicationDto> findContactLast(long contactId);
}
