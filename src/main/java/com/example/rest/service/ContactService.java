package com.example.rest.service;

import com.example.rest.model.Contact;

import java.util.Optional;

/**
 * Сервис контактов
 */
public interface ContactService {
    /**
     * Возвращает контакт по идентификатору
     *
     * @param id идентификатор контакта
     * @return контакт
     */
    Optional<Contact> findOne(long id);
}
