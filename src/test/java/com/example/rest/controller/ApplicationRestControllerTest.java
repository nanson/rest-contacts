package com.example.rest.controller;

import com.example.rest.model.Application;
import com.example.rest.model.Contact;
import com.example.rest.repository.ApplicationRepository;
import com.example.rest.repository.ContactRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WebAppConfiguration
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class ApplicationRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ObjectMapper objectMapper;

    private Contact contact;

    private List<Application> applicationList = new ArrayList<>();

    @Before
    public void setup() throws Exception {
        applicationRepository.deleteAllInBatch();
        contactRepository.deleteAllInBatch();

        contact = new Contact();
        contact.setId(1L);
        contact = contactRepository.save(contact);

        applicationList.add(createApplication(1L));
        applicationList.add(createApplication(2L));
    }

    @Test
    public void shouldFindLastApplication() throws Exception {
        Application application = applicationList.get(1);

        String createdAt = objectMapper.getDateFormat().format(application.getCreatedAt());

        mockMvc.perform(get("/contacts/{contactId}/applications/latest", contact.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.APPLICATION_ID", is(application.getId().intValue())))
                .andExpect(jsonPath("$.DT_CREATED", is(createdAt)))
                .andExpect(jsonPath("$.PRODUCT_NAME", is(application.getProductName())))
                .andExpect(jsonPath("$.CONTACT_ID", is(application.getContact().getId().intValue())));
    }

    @Test
    public void shouldThrowNotFoundExceptionForContact() throws Exception {
        mockMvc.perform(get("/contacts/{contactId}/applications/latest", -1L))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$[0].message",
                        is(String.format("Couldn't find contact with id %d", -1))));
    }

    @Test
    public void shouldThrowNotFoundExceptionForApplication() throws Exception {
        applicationRepository.deleteAllInBatch();
        mockMvc.perform(get("/contacts/{contactId}/applications/latest", contact.getId()))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$[0].message",
                        is(String.format("Couldn't find applications for contact with id = %d", contact.getId()))));
    }

    private Application createApplication(long id) {
        Application application = new Application();
        application.setContact(contact);
        application.setId(id);
        application.setProductName(String.format("productName-%d", id));
        return applicationRepository.save(application);
    }
}