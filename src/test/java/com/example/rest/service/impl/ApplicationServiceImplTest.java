package com.example.rest.service.impl;

import com.example.rest.dto.ApplicationDto;
import com.example.rest.model.Application;
import com.example.rest.model.Contact;
import com.example.rest.repository.ApplicationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationServiceImplTest {

    @InjectMocks
    private ApplicationServiceImpl applicationService;

    @Mock
    private ApplicationRepository applicationRepository;

    @Test
    public void shouldFindContactLast() {
        Application application = createApplication();
        when(applicationRepository.findTopByContactIdOrderByCreatedAtDesc(anyLong()))
                .thenReturn(Optional.of(application));

        Optional<ApplicationDto> applicationDtoOptional = applicationService.findContactLast(1L);

        assertTrue(applicationDtoOptional.isPresent());

        ApplicationDto applicationDto = applicationDtoOptional.get();
        assertEquals(application.getId(), applicationDto.getId());
        assertEquals(application.getCreatedAt(), applicationDto.getCreatedAt());
        assertEquals(application.getProductName(), applicationDto.getProductName());
        assertEquals(application.getContact().getId(), applicationDto.getContactId());
    }

    private Application createApplication() {
        Contact contact = new Contact();
        contact.setId(1L);

        Application application = new Application();
        application.setId(2L);
        application.setCreatedAt(new Date());
        application.setProductName("productName");
        application.setContact(contact);

        return application;
    }
}