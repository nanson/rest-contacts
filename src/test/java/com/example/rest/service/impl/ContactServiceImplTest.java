package com.example.rest.service.impl;

import com.example.rest.model.Contact;
import com.example.rest.repository.ContactRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceImplTest {

    @InjectMocks
    private ContactServiceImpl contactService;

    @Mock
    private ContactRepository contactRepository;


    @Test
    public void shouldFindContact() {
        Contact contact = new Contact();
        Optional<Contact> contactOptional = Optional.of(contact);
        when(contactRepository.findById(anyLong())).thenReturn(contactOptional);

        Optional<Contact> result = contactService.findOne(1L);

        assertEquals(contactOptional, result);
    }
}